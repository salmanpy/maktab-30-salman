<?php


namespace App\Http\Controllers;


use Illuminate\Support\Facades\Storage;

class ReturnNameController extends Controller
{
    public $name = "salman";

//    /**
//     * @param $jesus
//     * @return mixed
//     */
//    public function returnName($jesus)
//    {
//        return $jesus;
//    }

    /**
     * @param string $name
     * @return string
     */
    public function helloUser($name = "Laravel")
    {
        return "Hello " . $name;
    }

    #read text file
    public function read()
    {
        $content = Storage::get("public/textFile.txt");
        dd($content);
    }
}