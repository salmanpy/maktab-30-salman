<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DataController extends Controller
{
    public function create()
    {
        $files = glob(public_path('js/*'));
        \Zipper::make(public_path('test.zip'))->add($files)->close();
        return response()->download(public_path('test.zip'));
    }

    public function index()
    {
        $Path = public_path('test.zip');
        \Zipper::make($Path)->extractTo('not an empty folder');
    }
}
