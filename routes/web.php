<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

# new lines by me

#url = /returnName, event = return "my name is salman";
//Route::get('/returnName', function () {
//    return "my name is salman";
//});

#url = /new/{name}, name = parameter in url
//Route::get("/new/{jesus}", "ReturnNameController@returnName");

#url = user/{id}(parameter) event = return 'User ' . $id;

#you must use this(commenting)
///**
// * @param $id
// * @return string
// */
//Route::get('user/{id}', function ($id) {
//    return 'User ' . $id;
//});

//# optional parameter
//Route::get('user/{name?}', function ($name = null) {
//    return $name;
//});
//
//# optional parameter and a not optional parameter
//Route::get('jesus/{test}/{name?}', function ($test, $name = "salman") {
//    return $test . " || " . $name;
//});

//Route::get('helloUser/{name?}', function ($name) {
//    return $name;
//});
//

#1
//Route::get('create', 'DataController@create');
//Route::get('index', 'DataController@index');
#2
//Route::get('user/{id}', function ($id) {
//    return $id . "[A-Za-z]+";
//})->where('id', '[A-Za-z]+');
#3
//Route::get('user/{id}', function ($id) {
//    return $id . "[0-9]+";
//})->where('id', '[0-9]+');
#4
//Route::get('user/{id?}/{name?}', function ($id = 123, $name = "salman") {
//    return $id . "<br/>" . $name;
//})->where(['id' => '[0-9]+', 'name' => '[a-z]+']);
#5 just checking the storage path usage
//Route::get('user/{id?}', function ($id) {
//    $path = storage_path($id);
//    return $path;
//});
#6  read text file
Route::get('read', 'ReturnNameController@read');